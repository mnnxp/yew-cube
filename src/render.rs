use std::cell::RefCell;
use std::rc::Rc;

use wasm_bindgen::prelude::*;
use wasm_bindgen::JsCast;
// use web_sys::WebGlProgram;
// use web_sys::WebGlShader;
// use web_sys::WebGlBuffer;
// use web_sys::WebGlUniformLocation;
// use web_sys::WebGlVertexArrayObject;
use web_sys::HtmlCanvasElement;
// use web_sys::HtmlCollection;
// use web_sys::HtmlElement;
use web_sys::{Window, Document};
use web_sys::WebGl2RenderingContext as GL;

use crate::handles::start;

fn window() -> Window {
    web_sys::window().expect("no global `window` exists")
}

fn request_animation_frame(f: &Closure<dyn FnMut()>) {
    window()
        .request_animation_frame(f.as_ref().unchecked_ref())
        .expect("should register `requestAnimationFrame` OK");
}

fn document() -> Document {
    window()
        .document()
        .expect("should have a document on window")
}

pub fn render_gl(
    _gl: GL,
    width: u32,
    height: u32
) {
    let canvas = document().get_element_by_id("canvasgl").unwrap();
    let canvas = canvas.dyn_into::<HtmlCanvasElement>().unwrap();
    start(&canvas);

    // Gloo-render's request_animation_frame has this extra closure
    // wrapping logic running every frame, unnecessary cost.
    // Here constructing the wrapped closure just once.
    let cb = Rc::new(RefCell::new(None));
    let mut i = 0;
    *cb.borrow_mut() = Some(Closure::wrap(Box::new({
        let cb = cb.clone();
        move || {
            // Set the body's text content to how many times this
            // requestAnimationFrame callback has fired.
            i += 1;
            let text = format!(
                "width {}; height {}; requestAnimationFrame has been called {} times.",
                width,
                height,
                i
            );
            if let Some(text_info) = document().get_element_by_id("text-info") {
                text_info.set_inner_html(&text);
            }

            // Schedule ourself for another requestAnimationFrame callback.
            request_animation_frame(cb.borrow().as_ref().unwrap());
        }
    }) as Box<dyn FnMut()>));

    request_animation_frame(cb.borrow().as_ref().unwrap());
}