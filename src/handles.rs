use wasm_bindgen::prelude::*;
use web_sys::{WebGl2RenderingContext as GL, WebGlProgram, WebGlShader};

pub(crate) fn start(canvas: &web_sys::HtmlCanvasElement) {
    let context = canvas
        .get_context("webgl2")
        .unwrap()
        .unwrap()
        .dyn_into::<GL>()
        .unwrap();

    let vert_code = include_str!("./basic.vert");
    let frag_code = include_str!("./basic.frag");

    let vert_shader = compile_shader(
        &context,
        GL::VERTEX_SHADER,
        vert_code,
    )
    .unwrap();

    let frag_shader = compile_shader(
        &context,
        GL::FRAGMENT_SHADER,
        frag_code,
    )
    .unwrap();
    let program = link_program(&context, &vert_shader, &frag_shader).unwrap();
    context.use_program(Some(&program));

    let vertices: Vec<f32> = vec!(-0.7, -0.7, 0.0, 0.7, -0.7, 0.0, 0.0, 0.7, 0.0);
    let position_attribute_location = init_buffer_array_f32(&context, &program, &vertices, "position") as u32;

    let vao = context
        .create_vertex_array()
        .ok_or("Could not create vertex array object")
        .unwrap();
    context.bind_vertex_array(Some(&vao));

    context.vertex_attrib_pointer_with_i32(
        position_attribute_location,
        3,
        GL::FLOAT,
        false,
        0,
        0,
    );
    context.enable_vertex_attrib_array(position_attribute_location);

    context.bind_vertex_array(Some(&vao));

    let vert_count = (vertices.len() / 3) as i32;
    draw(&context, vert_count);
}

fn draw(context: &GL, vert_count: i32) {
    context.clear_color(0.0, 0.0, 0.0, 1.0);
    context.clear(GL::COLOR_BUFFER_BIT);

    context.draw_arrays(GL::TRIANGLES, 0, vert_count);
}

pub fn compile_shader(
    context: &GL,
    shader_type: u32,
    source: &str,
) -> Result<WebGlShader, String> {
    let shader = context
        .create_shader(shader_type)
        .ok_or_else(|| String::from("Unable to create shader object"))
        .unwrap();
    context.shader_source(&shader, source);
    context.compile_shader(&shader);

    if context
        .get_shader_parameter(&shader, GL::COMPILE_STATUS)
        .as_bool()
        .unwrap_or(false)
    {
        Ok(shader)
    } else {
        Err(context
            .get_shader_info_log(&shader)
            .unwrap_or_else(|| String::from("Unknown error creating shader")))
    }
}

pub fn link_program(
    context: &GL,
    vert_shader: &WebGlShader,
    frag_shader: &WebGlShader,
) -> Result<WebGlProgram, String> {
    let program = context
        .create_program()
        .ok_or_else(|| String::from("Unable to create shader object"))
        .unwrap();

    context.attach_shader(&program, vert_shader);
    context.attach_shader(&program, frag_shader);
    context.link_program(&program);

    if context
        .get_program_parameter(&program, GL::LINK_STATUS)
        .as_bool()
        .unwrap_or(false)
    {
        Ok(program)
    } else {
        Err(context
            .get_program_info_log(&program)
            .unwrap_or_else(|| String::from("Unknown error creating program object")))
    }
}

pub fn init_buffer_array_f32(
    context: &GL,
    program: &WebGlProgram,
    vertices: &Vec<f32>,
    attribute: &str
) -> i32 {
    let attribute_location = context.get_attrib_location(&program, attribute);
    let buffer = context.create_buffer().ok_or("Failed to create buffer").unwrap();
    context.bind_buffer(GL::ARRAY_BUFFER, Some(&buffer));

    context.buffer_data_with_array_buffer_view(
        GL::ARRAY_BUFFER,
        &js_sys::Float32Array::from(vertices.as_slice()),
        GL::STATIC_DRAW,
    );

    attribute_location
}