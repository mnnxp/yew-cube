pub mod app;
pub mod render;
pub mod handles;
// pub mod draw_scene;
// pub mod init_buffers;
// pub mod matrix;

use crate::app::App;

fn main() {
    yew::Renderer::<App>::new().render();
}
