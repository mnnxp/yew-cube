use wasm_bindgen::prelude::*;
use web_sys::{HtmlCanvasElement, WebGl2RenderingContext};
use yew::{html, Component, Context, Html, NodeRef};

use crate::render::render_gl;

pub const CANVAS_WIDTH: u32 = 640;
pub const CANVAS_HEIGHT: u32 = 480;

// Wrap gl in Rc (Arc for multi-threaded) so it can be injected into the render-loop closure.
pub struct App {
    node_ref: NodeRef,
}

impl Component for App {
    type Message = ();
    type Properties = ();

    fn create(_ctx: &Context<Self>) -> Self {
        Self {
            node_ref: NodeRef::default(),
        }
    }

    fn view(&self, _ctx: &Context<Self>) -> Html {
        html! {<>
            <p id={"text-info"}></p>
            <canvas id={"canvasgl"} ref={self.node_ref.clone()} />
        </>}
    }

    fn rendered(&mut self, _ctx: &Context<Self>, first_render: bool) {
        // Only start the render loop if it's the first render
        // There's no loop cancellation taking place, so if multiple renders happen,
        // there would be multiple loops running. That doesn't *really* matter here because
        // there's no props update and no SSR is taking place, but it is something to keep in
        // consideration
        if !first_render {
            return;
        }
        // Once rendered, store references for the canvas and GL context. These can be used for
        // resizing the rendering area when the window or canvas element are resized, as well as
        // for making GL calls.
        let canvas = self.node_ref.cast::<HtmlCanvasElement>().unwrap();
        canvas.set_attribute("width", CANVAS_WIDTH.to_string().as_str()).unwrap();
        canvas.set_attribute("height", CANVAS_HEIGHT.to_string().as_str()).unwrap();
        let width = canvas.get_attribute("width").unwrap_or_default().parse::<u32>().unwrap_or(CANVAS_WIDTH);
        let height = canvas.get_attribute("height").unwrap_or_default().parse::<u32>().unwrap_or(CANVAS_HEIGHT);
        let gl: WebGl2RenderingContext = canvas
            .get_context("webgl2")
            .unwrap()
            .unwrap()
            .dyn_into()
            .unwrap();
        render_gl(
            gl,
            width,
            height
        );
    }
}